import pybullet_data
import pybullet as p
import math
import time
from datetime import datetime
from pathlib import Path

from src.robot_actions.robot_action_base import RobotActionFinishedException, RobotActionCancelException
from src.robot_actions.arm_movement_robot_actions import RobotArmMovement
from src.robot_actions.move_gripper_fingers_robot_actions import MoveGripperFingersForceControl
from src.robot_actions.robot_action_queue import RobotActionQueue
from src.robot_actions.robot_actions_helper_functions import _gripperOpeningWidth, _gripperForce
from src.joint_forces_data import TCPData, GripperData, OrnQuaternion, Position, OrnEuler


class StopSimulationException(Exception):
    pass


class UR10_RL(object):

    def __init__(self, renderGUI=True, maxSimulationIterations=20, maxGripAdjustments=1):
        #typical initializing, pybullet-related

        #self.maxGripAdjustments defines, how many times the robot should do a re-grab, based
        # on initial grab, which results in forces.
        self.maxGripAdjustments=maxGripAdjustments
        self.gripAdjustmentsCounter = -1
        
        self.renderGUI = renderGUI
        if self.renderGUI is True:
            p.connect(p.GUI)
        else:
            p.connect(p.DIRECT)
            
        p.configureDebugVisualizer(p.COV_ENABLE_RENDERING,0)
        p.configureDebugVisualizer(p.COV_ENABLE_SINGLE_STEP_RENDERING)
        # p.setAdditionalSearchPath(pybullet_data.getDataPath())
        p.setRealTimeSimulation(0)
        p.setGravity(0,0,-9.81)
        self.stepFrequency = 240
        p.setTimeStep(1/self.stepFrequency)
        p.setPhysicsEngineParameter(reportSolverAnalytics=True)
        p.resetDebugVisualizerCamera(cameraDistance=1.5, cameraYaw=0, cameraPitch=-40, cameraTargetPosition=[0.55,-0.35,0.2])

        plane_path = Path("urdf/plane/plane.urdf")
        self.planeId = p.loadURDF(str(plane_path))
        # self.planeId = p.loadURDF(str(plane_path.relative_to(plane_path.parent.parent)))
        
        
        robot_path = Path(f"urdf/ur10_realsense_wsg50_no_limits.urdf")
        # robot_path = Path(f"pybullet_pick_and_place/urdf/ur10_realsense_wsg50_no_limits.urdf")
        self.robotId = p.loadURDF(str(robot_path), [0,0,0], p.getQuaternionFromEuler([0,0,0,]), useFixedBase=1)
        p.resetBasePositionAndOrientation(self.robotId, [0,0,0],[0,0,0,1])

        # tray_path = Path("tray/tray.urdf")
        # self.tray = p.loadURDF(str(tray_path.relative_to(tray_path.parent.parent)), [0.5,-0.5,0], useFixedBase=0)
        self.tcpJointIndex = 6
        p.enableJointForceTorqueSensor(self.robotId, self.tcpJointIndex, True)
        
        #counter of current simulation & upper limit of simulations
        self.simulationCounter = 1
        self.maxSimulationIterations = maxSimulationIterations


        #loading of robot, plane, try as urdf-files
        self.trayDropPosition = [0.5,-0.5,0.3]

        #for controlling the movement: eeLinkIndex is a visual cube between the fingers
        self.endEffectorLinkIndex = 12
        
        #relevant for logging tcp data
        self.startParam = p.addUserDebugParameter(paramName="startParam", rangeMin=0, rangeMax=1, startValue=0)

        #indices of joints/ links
        self.ARM_JOINTS = (0,1,2)
        self.WRIST_JOINTS = (3,4,5)
        self.FINGER_JOINTS = (8,10)
        self.FINGER_LINKS = (8,11) #left, right
        self.numActiveJoints = len(self.ARM_JOINTS + self.WRIST_JOINTS + self.FINGER_JOINTS)
        self.activeJoints = self.ARM_JOINTS + self.WRIST_JOINTS + self.FINGER_JOINTS

        self.objects = []
        self.actionQueue = RobotActionQueue()
        
        #frequency in Hz of data logging process
        self.dataFrequency = 240
        if self.stepFrequency % self.dataFrequency != 0 or self.dataFrequency > self.stepFrequency:
            raise Exception("""Implementation Error: 
        stepFrequency must be a multiple from the desired data logging frequency 
        and must be greater than dataFrequency.""")
        
        #resting pose of each joint
        self.restPose = [
            0,
            -math.pi/2,
            math.pi/2,
            -math.pi/2,
            math.pi/2,
            math.pi,
            0.053,
            -0.053
        ]

        #jd: Joint dampings, ll: LowerLimits, ul: UpperLimits, jr: JointRanges
        self.jd = []
        self.ll = [-4, -4,-4,-4, -4,-2, 0.0015, -0.054]
        self.ul = [4,4,4,4,4,4, 0.054, -0.0015]
        self.jr = [ self.ul[i]-self.ll[i] for i in range(len(self.ll))]

        #setup joint max forces/ velocities
        self.jointMaxForces = []
        self.jointMaxVelocities = []

        for i in range(self.numActiveJoints):
            info = p.getJointInfo(self.robotId, self.activeJoints[i])
            self.jointMaxForces.append(info[10])
            self.jointMaxVelocities.append(info[11])
            print(f"Joint with Index {self.activeJoints[i]}: maxForce={info[10]}, maxVelocity = {info[11]}")

        self._resetSimulation()
        print("#"*20)
        print("FINISHED INITIALIZING", self.actionQueue)
        for i in self.actionQueue.actions:
            print(i.__str__())
        print("#"*20)




    def _getGripperData(self):
        gripperForce = _gripperForce(self)
        openingWidth = _gripperOpeningWidth(self)

        link_state = p.getLinkState(self.robotId, self.endEffectorLinkIndex)

        ornQuat = link_state[1]
        ornEuler = p.getEulerFromQuaternion(ornQuat)

        return GripperData(
            stepCounter=self.stepCounter, 
            simulationCounter=self.simulationCounter, 
            force=gripperForce, 
            openingWidth=openingWidth, 
            eeGuidePos=Position(*link_state[0]),
            eeGuideOrnQuat=OrnQuaternion(*ornQuat),
            eeGuideOrnEuler=OrnEuler(*ornEuler)
        )
    
    def _getTCPData(self):
        tcpState = p.getJointState(self.robotId, self.tcpJointIndex)[2]
        fx=round(tcpState[0],4)
        fy=round(tcpState[1],4)
        fz=round(tcpState[2],4)
        mx=round(tcpState[3],4)
        my=round(tcpState[4],4)
        mz=round(tcpState[5],4)

        tcpPos = Position(*p.getLinkState(self.robotId, 6)[4])

        #tcp orientation
        _tcpOrn = p.getLinkState(self.robotId,6)[5]
        tcpOrnQuat = OrnQuaternion(*_tcpOrn)
        tcpOrnEuler = OrnEuler(*p.getEulerFromQuaternion(_tcpOrn))

        return TCPData(
            stepCounter=self.stepCounter, 
            simulationCounter=self.simulationCounter, 
            fx=fx,
            fy=fy,
            fz=fz,
            mx=mx,
            my=my,
            mz=mz,
            tcpPos=tcpPos,
            tcpOrnQuat=tcpOrnQuat,
            tcpOrnEuler=tcpOrnEuler)
        
    

    
    def _jointForces(self):
        """Read out forces/positions of gripper and tcp and returns them.

        Returns:
        tuple: Returns 2-tuple with GripperData- and TCPData - instances. 
        """
        multiple = self.stepFrequency // self.dataFrequency
        
        if self.stepCounter % multiple == 0:
            return self._getGripperData(), self._getTCPData()

    
    def _debugLine(self):
        self.previous_ee_pos = self.current_ee_pos
        self.current_ee_pos = p.getLinkState(self.robotId,self.endEffectorLinkIndex)[4]
        p.addUserDebugLine(self.previous_ee_pos, self.current_ee_pos, [1,0,0], 3, 5)
    
    

    def fillActionQueue(self):
        pos = [0.4, 0.4, 0]
        # orn = [0,0,0.5*math.pi*a/180]
        orn = [0,0,0]
        
        
        cube_path = Path("urdf/cube.urdf")
        cube = p.loadURDF(str(cube_path),pos,p.getQuaternionFromEuler(orn))
        self.objects = [(cube, pos),]

        gripperOrn = [0.5*math.pi/180*1,0.0,0.0]

        self.actionQueue.addActionsToQueue(
            RobotArmMovement(self, pos, gripperOrn),
            # MoveGripperFingers(self, isOpening=False),
        )
    
    def _resetSimulation(self):
        """
        Resets simulation. Objects are restored, robot takes resting pose.
        """
        #clean-up first
        for i in range(len(self.objects)):
            obj, pos = self.objects[i]
            p.removeBody(obj)
        
        self.current_ee_pos = p.getLinkState(self.robotId, self.endEffectorLinkIndex)[4]
        self.previous_ee_pos = self.current_ee_pos
    
        for i in range(len(self.restPose)):
            p.resetJointState(self.robotId,self.activeJoints[i],self.restPose[i])
        
        self.actionQueue.actions = []
        self.fillActionQueue()

        #number of calculation steps of each simulation
        self.stepCounter = 0

        p.configureDebugVisualizer(p.COV_ENABLE_RENDERING,1)

        print("-"*5)
        print("Resetting simulation: gripAdjustmentsCounter = ", self.gripAdjustmentsCounter)

    

    
    def _performAction(self):
        """
        Called every sim cycle and calls <current_action>.doAction() every time.
        """
        self.stepCounter += 1
        action = self.actionQueue.actions[0]
        action.doAction()

        self._debugLine()
        self._jointForces()

    def _repeatSimulation(self):
        """
        Called when all actions are done. Resets simulation if simulationCounter < maxSimulationIterations.
        """
        #action Queue is empty, decide whether we do another sim iteration or not
        # print("NO MORE ACTIONS HERE FOR CURRENT SIMULATION INSTANCE")
        # print("Number of steps are:", self.stepCounter)

        
        print(f"Starting next round. {self.simulationCounter} / {self.maxSimulationIterations} Simulation iterations have finished.")
        if self.simulationCounter <= self.maxSimulationIterations:
            #re-iterate
            self._resetSimulation()
        else:
            raise StopSimulationException("The number of iterations has reached its limit.")
        self.simulationCounter += 1
        
    def _handleRobotActionFinishedException(self,e):
        raise Exception("This function must be overwritten")

    def _handelRobotActionCancelException(self,e):
        raise Exception("This function must be overwritten")

    def startStepping(self):
        """
        Loop for stepSimulations(). Gets the first object from actionQueue and calls doAction() on it. After the action is fullfilled, 
        the action removes itself from the queue and the following action will be called with doActio() 
        """

        while True:
            try:
                if self.renderGUI is True:
                    start = p.readUserDebugParameter(self.startParam)
                else:
                    start = 1
                if start > 0:

                    if len(self.actionQueue.actions) == 0:
                       self._repeatSimulation() 

                    else:
                        self._performAction()

                simulationValues = p.stepSimulation()

            except KeyboardInterrupt:
                break
            
            except RobotActionFinishedException as e:
                self._handleRobotActionFinishedException(e)

            except RobotActionCancelException as e:
                self._handelRobotActionCancelException(e)

            except StopSimulationException:
                break
                

        p.disconnect()
    

if __name__ == "__main__":
    robot = UR10_RL()
    robot.startStepping()