# Pybullet Environment 

In der ´README.md´ geht es nur um die Installation der Umgebung. Weitere Infos für die Benutzung des Codes befinden sich unter `documentation/` 

## Installation:
Es gibt zwei verschiedene Arten der Installation:
- pip
- Anaconda 

### pip - Installation:

Python/ Python3 und git muss installiert sein. Admin-Rechte erforderlich

Die folgende Anweisung sollte benötigte externe Packete installieren.
Sollte was fehlen: `pip install <name-of-package>`. Wichtig: Immer in aktivierter Python-Umgebung arbeiten, sonst können die Packages nicht gefunden werden (beim Benutzen bzw. werden nicht in die Umgebung installiert, sondern systemweit), d.h. es wird neben dem Code-Editor ein Terminal-Fenster benutzt werden.

#### Windows:

1) Windows Terminal oder cmd-Fenster öffnen, mittels `cd ...` in Zielordner bewegen
2) Repository Clonen: `git clone git@git.rwth-aachen.de:robin_rob96/pybullet-environment.git`
3) `cd pybullet-environment`
4) Python Umgebung erstellen: Alle zu installierenden Packete werden in dieser Umgebung installiert und beeinflussen damit nicht das Sytem. `python -m venv env`, wobei "env" der Name der Umgebung ist.
5) Umgebung aktivieren: `env\Scripts\activate`
6) Externe Packete installieren: `pip install -r requirements.txt`

#### Mac OS / Linux:

1) Terminal öffnen, mittels `cd ...` in Zielordner bewegen
2) Repo clonen: `git clone git@git.rwth-aachen.de:robin_rob96/pybullet-environment.git`
3) `cd pybullet-environment`
4) Python Umgebung erstellen: Alle zu installierenden Packete werden in dieser Umgebung installiert und beeinflussen damit nicht das Sytem. `python -m venv env`, wobei "env" der Name der Umgebung ist.
5) Umgebung aktivieren: `source env/bin/activate`
6) Externe Packete installieren: `pip install -r requirements.txt`


### Anaconda - Installation unter Windows:

Diese Anleitung bezieht sich auf Windows.
Zuerst muss der Anaconda - Navigator installiert werden.

1) Im Anaconda-Navigator links im Reiter `Environment` und dann unten mit `create` eine neue Umgebung mit Python anlegen und diese z.B. pybullet-env nennen
2) Zurück auf den Reiter `Home` und oben die eben erstelle Umgebung auswählen
3) Nun ein Cmd-Fenster (über Anaconda) öffnen, der Name der eben erstellen Umgebung sollte links in Klammern stehen
4) gewünschten Zielordner auswählen `cd ...`
5) Mittels git clonen: `git clone git@git.rwth-aachen.de:robin_rob96/pybullet-environment.git`
6) Externe Packete installieren: `pip install -r requirements.txt`

Damit sollte nun ebenfalls alles benötigte installiert sein. Zum benutzten muss das Cmd-Fenster immer über Anaconda gestartet werden, damit direkt die richtige Umgebung aktiviert ist.