from ur10_RL_wrapper_multiprocessing import UR10WrapperRLMultiprocessing
from src.agent import LearningAgent
from multiprocessing import SimpleQueue, Process, Pipe
import argparse


def writer_proc(data_queue, conn, sim_iters):
    UR10WrapperRLMultiprocessing(data_queue=data_queue,conn=conn,sim_iterations=sim_iters, gui=False, rt_simulation=True)




if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description="Reinforcement Learning with Pybullet.")
    
    parser.add_argument('--batch_size', type=int, default=25, metavar='B', help="Batch size for learning. (default: 25)")
    parser.add_argument('--num_proc', type=int, default=1, metavar='P', help="Number of parallel computed simulations. (default: 1)")
    parser.add_argument('--sim_iterations', type=int, default=1000, metavar='I', help="Number of simulation iterations of each parallel computed process. (default: 1000)")
    parser.add_argument('--continue_log', default=False, action="store_true", help="Should log be continued? Important for TensorboardX. (default:False)")
    args = parser.parse_args()

    batch_size = vars(args)['batch_size']
    num_proc = vars(args)['num_proc']
    sim_iterations = vars(args)['sim_iterations']
    continue_log = vars(args)['continue_log']

    agent = LearningAgent(batch_size, continue_logfile=continue_log)
    data_q = SimpleQueue()
    pipes = []

    for i in range(num_proc):
        conn1, conn2 = Pipe()

        p_writer = Process(target=writer_proc, args=(data_q,conn2, sim_iterations))
        p_writer.start()
        pipes.append(conn1)


    while True:

        if not data_q.empty():
            data = data_q.get()
            agent.memory.add_data(data)

        for p in pipes:
            if p.poll():
                
                state_tensor, action_tensor = p.recv()
                action_value, action_index = agent.get_action_with_highest_exp_reward(state_tensor, action_tensor)
                p.send((action_value, action_index))

        agent.optimize_model()