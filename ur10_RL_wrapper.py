import pybullet as p
from pathlib import Path
from random import uniform
import random
from math import pi, exp
import numpy as np
import argparse

from ur10_RL import UR10_RL
from src.robot_actions.move_gripper_fingers_robot_actions import MoveGripperFingersForceControl, MoveGripperFingersDistanceControl, _MoveGripperFingersBase
from src.robot_actions.arm_movement_robot_actions import RobotArmMovement, RobotArmMovementWithLoad
from src.robot_actions.robot_action_base import RobotActionFinishedException
from src.joint_forces_data import TCPData, GripperData, OrnQuaternion, Position, OrnEuler
from src.agent import LearningAgent


import torch
from torch.autograd import Variable
from torch import functional as F

class UR10WrapperClassRL(UR10_RL):
    
    def _init_vars(self):
        self.cube_pos = None
        self.cube_orn = None

        self.gripperGuideOrn = None
        self.futureGripperGuideOrn = None

        self.lastAdjustGripAction = None
        self.lastGripForces = None


    def __init__(self, sim_iterations=50, gui=False, rt_simulation=False, model_name=None):
        self._init_vars()
        
        super().__init__(maxSimulationIterations=sim_iterations, renderGUI=gui, maxGripAdjustments=1)
        p.setRealTimeSimulation(int(rt_simulation))
        p.resetDebugVisualizerCamera(cameraDistance=1.0, cameraYaw=100, cameraPitch=-15, cameraTargetPosition=[0.7,0.3,0.2])
        
        self.agent = LearningAgent(batch_size=10, model_name=model_name, continue_logfile=False)

        self.startStepping()

    def _resetSimulation(self):

        try:
            self.agent.evaluate_model()
        except:
            pass

        if self.gripAdjustmentsCounter >= self.maxGripAdjustments:
            self.gripAdjustmentsCounter = -1
            
        super()._resetSimulation()

    
    def get_random_obj_pos(self):
        """Get random value for y, then for radius. Calculate x with pythagoras. 
        Because of (r**2 - y**2), r >= abs(y), so r = max( 0.3 , abs(y)) with 0.3 is min radius for grabbing object close to robot."""
        y = uniform(-0.8,0.8)    
        r = uniform(
            max(0.3,abs(y))
            ,0.8)
        x = np.sqrt(r**2 - y**2)
        z = 0        
        return (x, y, z)

    
    def get_random_obj_orn(self):
        return [0,0,uniform(0, pi/2)]

    
    def get_random_guide_orn(self):
        return [
            uniform(-20*pi/180, 20*pi/180),
            uniform(-20*pi/180, 20*pi/180),
            uniform(-20*pi/180, 20*pi/180),
            ]
    
    
    def model_exploitation(self):
        all_possible_actions = self.get_all_actions()
        
        action_tensor = torch.Tensor(all_possible_actions)
        state_tensor = torch.Tensor([self.lastGripForces]*len(all_possible_actions))
        
        action_value,action_index = self.agent.get_action_with_highest_exp_reward(state_tensor, action_tensor)
        action = all_possible_actions[action_index]
        return action

    
    def model_training(self):
        EPS_START = 0.9
        EPS_END = 0.1
        EPS_DECAY = 400

        eps_treshold = EPS_END + (EPS_START-EPS_END)*exp(-1*self.simulationCounter/EPS_DECAY)
        exploration = random.random()
        
        if exploration < eps_treshold:
            #exploration
            action = random.choice(self.get_all_actions())
        else:
            #exploitation
            action = self.model_exploitation()
        return action

    
    
    def _get_action(self):
        """This method is used for an easy change of fillActionQueue when subclassing. 
        In this version, we don't want to train the model but only exploit it.
        """
        return self.model_exploitation()

        
    def fillActionQueue(self):
        """Gets called once simulation is resetted."""

        if self.gripAdjustmentsCounter == -1:
            #add initial actions to queue.
            self.cube_pos = self.get_random_obj_pos()
            self.cube_orn = self.get_random_obj_orn()
            self.gripperGuideOrn = self.get_random_guide_orn()
            self.futureGripperGuideOrn = self.gripperGuideOrn
        else:
            action = self._get_action()

            self.lastAdjustGripAction = action
            self.futureGripperGuideOrn = [sum(x) for x in zip(self.gripperGuideOrn, action)]

        self.actionQueue.addActionsToQueue(
            RobotArmMovement(self, self.cube_pos[:2]+(0.1,), self.futureGripperGuideOrn),
            RobotArmMovement(self, self.cube_pos[:2]+(0.005,), self.futureGripperGuideOrn),
            MoveGripperFingersForceControl(self, isOpening=False, fingerForce=100,cancelActionDelay=5),
        )

        cube_path = Path("urdf/cube.urdf")
        cube = p.loadURDF(str(cube_path),self.cube_pos,p.getQuaternionFromEuler(self.cube_orn))
        self.objects = [(cube, self.cube_pos),]

        self.gripAdjustmentsCounter += 1


    def get_all_actions(self):
        return  [
            (dx*pi/180, dy*pi/180, dz*pi/180) 
            for dx in range(-20,21,2) 
            for dy in range(-20,21,2) 
            for dz in range(-45,46,2)]
    
    
    def _push_data_to_memory(self,t):
        """Used for modifying behaviour when subclassed."""
        self.agent.memory.add_data(t)
    
    
    def validate_gripper_data(self, gripperData, tcpData):
        if gripperData is None or tcpData is None:
            return

        if self.gripAdjustmentsCounter > 0:
            print("--- validate_gripper_data")
            reward = self.calculate_reward(gripperData)
            # forces[t-1]; action[t-1], reward[t] --> because forces and choosen action lead to new grip of robot arm which yields reward afterwards
            t = (self.lastGripForces, self.lastAdjustGripAction, reward)
            # self.agent.memory.add_data(t)
            self._push_data_to_memory(t)
        
        self.lastGripForces = tcpData.get_forces_tuple()
        self.gripperGuideOrn = self.futureGripperGuideOrn

    def calculate_reward(self, gripperData):
        cube, _ = self.objects[0]
        cubeQuat = p.getBasePositionAndOrientation(cube)[1]
        cubeMat = p.getMatrixFromQuaternion(cubeQuat)
        cubeMat = np.array([cubeMat[:3], cubeMat[3:6], cubeMat[6:9]])

        # GuideMat * M = CubeMat
        # <=> M = GuideMat^-1 * CubeMat

        guideQuat = gripperData.eeGuideOrnQuat.get_as_list()
        guideMat = p.getMatrixFromQuaternion(guideQuat)
        guideMat = np.array([guideMat[:3], guideMat[3:6], guideMat[6:9]])

        guide_inv = np.linalg.inv(guideMat)
        diff_mat = np.matmul(guide_inv, cubeMat)

        thetaX = np.arctan2(diff_mat[2,1], diff_mat[2,2])
        thetaY = np.arctan2(-diff_mat[2,0], np.sqrt(diff_mat[2,1]**2 + diff_mat[2,2]**2))
        thetaZ = np.arctan2(diff_mat[1,0], diff_mat[0,0])

        return -1*np.sqrt(thetaX ** 2 + thetaY ** 2 + thetaZ ** 2)


    def _handleRobotActionFinishedException(self, e):
        self.actionQueue.actions.pop(0)
        self.validate_gripper_data(e.gripperData, e.tcpData)
        

    def _handelRobotActionCancelException(self, e):
        self.actionQueue.actions.pop(0)
        if self.gripAdjustmentsCounter == 0:
            # initial grab for rendering data failed. Next reset should be inital grab again.
            self.gripAdjustmentsCounter = -1
        print("--- action Cancel!")



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Reinforcement Learning with Pybullet.")
    
    parser.add_argument('--n_iters', type=int, default=1000, metavar='I', help="Number of simulation iterations (default: 1000")
    parser.add_argument('--gui', default=False, action="store_true", help="Render gui while simulate (default: False)")
    parser.add_argument('--rt', default=False,  action="store_true",help="Simulation in real time (default: False)")
    parser.add_argument('--model_name', default=None,  type=str, help="Used for choosing one specific model, e.g. <my_model.pt>")

    args = parser.parse_args()

    n_sim_iters = vars(args)['n_iters']
    gui = vars(args)['gui']
    rt = vars(args)['rt']
    model_name = vars(args)['model_name']

    wrapper = UR10WrapperClassRL(n_sim_iters, gui, rt, model_name)

    

