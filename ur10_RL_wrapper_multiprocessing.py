from tensorboard.backend.event_processing.event_accumulator import ImageEvent
import torch
from ur10_RL_wrapper import UR10WrapperClassRL
from ur10_RL import UR10_RL
import pybullet as p
import time

class UR10WrapperRLMultiprocessing(UR10WrapperClassRL):
    
    def __init__(self, sim_iterations=50, gui=False, rt_simulation=False, model_name=None, data_queue=None, conn=None):
        self._init_vars()

        UR10_RL.__init__(self,maxSimulationIterations=sim_iterations, renderGUI=gui, maxGripAdjustments=1)
        p.setRealTimeSimulation(int(rt_simulation))
        p.resetDebugVisualizerCamera(cameraDistance=1.0, cameraYaw=100, cameraPitch=-15, cameraTargetPosition=[0.7,0.3,0.2])

        self.data_queue = data_queue
        self.conn = conn

        self.startStepping()

    def _resetSimulation(self):
        if self.gripAdjustmentsCounter >= self.maxGripAdjustments:
            self.gripAdjustmentsCounter = -1
            
        UR10_RL._resetSimulation(self)

    
    def model_exploitation(self):
        all_possible_actions = self.get_all_actions()
        state_tensor = torch.Tensor([self.lastGripForces]*len(all_possible_actions))
        action_tensor = torch.Tensor(all_possible_actions)

        self.conn.send((state_tensor, action_tensor))
        action_value, action_index = self.conn.recv()

        action = all_possible_actions[action_index]
        return action

    def _get_action(self):
        return self.model_training()

    def _push_data_to_memory(self, t):
        self.data_queue.put(t)

