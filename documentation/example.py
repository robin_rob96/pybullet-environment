import pybullet_data
import pybullet as p
from pathlib import Path
import math

p.connect(p.GUI)  
p.setAdditionalSearchPath(pybullet_data.getDataPath())
p.setRealTimeSimulation(1)
p.setGravity(0,0,-9.81)
p.setTimeStep(1/240)

plane_path = Path("plane.urdf")
planeId = p.loadURDF(str(plane_path.relative_to(plane_path.parent.parent)))

robot_path = Path(f"../urdf/ur10_realsense_wsg50_no_limits.urdf")
robotId = p.loadURDF(str(robot_path), [0,0,0], p.getQuaternionFromEuler([0,0,0,]), useFixedBase=1)

restPose = [
    0,
    -math.pi/2,
    math.pi/2,
    -math.pi/2,
    math.pi/2,
    math.pi,
    0.053,
    -0.053
    ]

activeJoints = (0,1,2,3,4,5,9,11)

for i in range(len(restPose)):
    p.resetJointState(robotId,activeJoints[i],restPose[i])

info_1 = p.getJointInfo(robotId, 6)[14]
info_2 = p.getJointInfo(robotId, 8)[14]

print(info_1, info_2)


while True:
    p.stepSimulation()