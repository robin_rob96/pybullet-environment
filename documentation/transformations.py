# import numpy as np
from sympy import symbols, Matrix, cos, sin
from math import pi

theta1, theta2, theta3, theta4, theta5, theta6 = symbols('theta1 theta2 theta3 theta4 theta5 theta6')

def rotation_mat(rotation_axis="x",angle=theta1):
    if rotation_axis == "x":
        return Matrix([[1,0,0],[0,cos(angle),-sin(angle)],[0, sin(angle),cos(angle)]])
    
    elif rotation_axis == "y":
        return Matrix([[cos(angle),0,sin(angle)],[0,1,0],[-sin(angle),0,cos(angle)]])
    
    elif rotation_axis == "z":
        return Matrix([[cos(angle),-sin(angle),0],[sin(angle),cos(angle),0],[0,0,1]])


#angles in resting position
theta1 = 0
theta2 = -pi/2
theta3 = pi/2
theta4 = -pi/2
theta5 = pi/2
theta6 = pi

#rotation matrices for 6 DoF's
mat_0_1 = rotation_mat("z",theta1) 
mat_1_2 = rotation_mat("y",theta2) * Matrix([[0,0,1],[0,1,0],[-1,0,0]])
mat_2_3 = rotation_mat("y",theta3)
mat_3_4 = rotation_mat("y",theta4) * Matrix([[0,0,1],[0,1,0],[-1,0,0]])
mat_4_5 = rotation_mat("z",theta5) * Matrix([[-1,0,0],[0,-1,0],[0,0,-1]])
mat_5_6 = rotation_mat("y",theta6)

#additional rotations done by rigid joints
mat_6_tcp = Matrix([[0,0,1],[1,0,0],[0,1,0]])
mat_wsg50_base_link_joint = Matrix([[0,1,0],[1,0,0],[0,0,-1]])
mat_tcp_guide = Matrix([[0,-1,0],[-1,0,0],[0,0,-1]])


# complete_transform = mat_0_1*mat_1_2*mat_2_3*mat_3_4*mat_4_5*mat_5_6*mat_6_tcp*mat_wsg50_base_link_joint*mat_tcp_guide
complete_transform = mat_0_1*mat_1_2*mat_2_3*mat_3_4*mat_4_5*mat_5_6*mat_6_tcp
#in the resting position, the guide COS is exactly oriented like the world COS
print(complete_transform)