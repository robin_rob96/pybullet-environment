# %%
import torch
import torch.nn as nn
from itertools import count
from pathlib import Path
from tensorboardX import SummaryWriter

import random
from src.memory import Memory
from src.neuronal_net import QLearningNN, QLearningNN2

NN_MODEL_NAME = "nn_model.pt"
LOG_DESTINATION = "logs/training"
LOG_DESCRIPTION_LEARNING_LOSS = "SGD_learning_loss"
LOG_DESCRIPTION_EVAL_LOSS = "SGD_eval_loss"

LOG_DESCRIPTION_EXPECTED_REWARD = "expected_reward"
LOG_DESCRIPTION_ACTION_INDEX_OF_MAX_EXP_REWARD = "action_index_of_max_exp_reward"


def _get_previous_log_count(continue_logfile,desc):
    if continue_logfile:
        from tensorboard.backend.event_processing.event_accumulator import EventAccumulator
        event_acc = EventAccumulator(LOG_DESTINATION)
        event_acc.Reload()
        _, step_nums,_ = zip(*event_acc.Scalars(desc))
        log_len = len(step_nums)
        return log_len
    return 0


class LearningAgent():

    def __init__(self, batch_size=25,model_name=None, continue_logfile=False):
        self.batch_size = batch_size
        self.memory = Memory(batch_size=self.batch_size)

        self.n_optim_model_iter = count(_get_previous_log_count(continue_logfile, LOG_DESCRIPTION_LEARNING_LOSS))
        self.n_max_action_reward = count(_get_previous_log_count(continue_logfile, LOG_DESCRIPTION_ACTION_INDEX_OF_MAX_EXP_REWARD))

        self.model_name = model_name if model_name is not None else NN_MODEL_NAME
        
        self.writer = SummaryWriter(LOG_DESTINATION)

        if Path(self.model_name).exists():
            self.model = torch.load(self.model_name)
            print("#"*10)
            print("LOADING MODEL WAS SUCCESSFULLY!")
        else:
            self.model = QLearningNN2()
        self.model.float()
        self.optimizer = torch.optim.SGD(self.model.parameters(), lr=0.35)
        self.model.train()


    def optimize_model(self):
        if len(self.memory.data) < self.batch_size: return

        print("###"*10)
        print("OPTIMIZE_MODEL")
        print("###"*10)

        state, action, target_reward = self.memory.get_samples()
        
        self.model.train()
        self.optimizer.zero_grad()

        reward = self.model(state, action)
        
        criterion = nn.SmoothL1Loss()
        loss = criterion(reward, target_reward)
        self.log_loss(loss, LOG_DESCRIPTION_LEARNING_LOSS)
        
        loss.backward()
        self.optimizer.step()
        torch.save(self.model, self.model_name)


    def evaluate_model(self):
        if len(self.memory.data) < self.batch_size: return

        self.model.eval()
        state, action, target_reward = self.memory.get_samples()

        with torch.no_grad():
            calc_reward = self.model(state, action)
            criterion = nn.SmoothL1Loss()
            loss = criterion(calc_reward, target_reward)
            self.log_loss(loss, LOG_DESCRIPTION_EVAL_LOSS)



    def get_action_with_highest_exp_reward(self, state, action):
        with torch.no_grad():
            reward = self.model(state, action)
            value, index = torch.max(reward,0)
            self.log_model_exploitation(value, index)
            return value,index
    
    
    def log_loss(self, loss, desc):
        self.writer.add_scalar(desc, loss, next(self.n_optim_model_iter))

    def log_model_exploitation(self, val, index):
        t = next(self.n_max_action_reward)
        self.writer.add_scalar(LOG_DESCRIPTION_EXPECTED_REWARD, val, t)
        self.writer.add_scalar(LOG_DESCRIPTION_ACTION_INDEX_OF_MAX_EXP_REWARD, index, t)
   