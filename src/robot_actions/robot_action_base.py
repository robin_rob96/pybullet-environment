from datetime import datetime


class RobotActionFinishedException(Exception):
    def __init__(self, message, gripperData=None, tcpData=None):
        super().__init__(self, message)
        self.gripperData = gripperData
        self.tcpData = tcpData


class RobotActionCancelException(Exception):
    def __init__(self, message, gripperData=None, tcpData=None):
        super().__init__(self, message)
        self.gripperData = gripperData
        self.tcpData = tcpData


class RobotAction(object):
    def __init__(self, robot,cancelActionDelay=5):
        self.robot = robot
        
        self.cancelActionDelay = cancelActionDelay
        self.cancelTimer = None

    def doAction(self):
        if self.cancelTimer is None:
            self.cancelTimer = datetime.now()

        if self.cancelTimer and (datetime.now() - self.cancelTimer).total_seconds() > self.cancelActionDelay:
            # self.actionFinished()
            gripperData = self.robot._getGripperData()
            tcpData = self.robot._getTCPData()
            raise RobotActionCancelException("Stop execution due to cancelTimer.", gripperData, tcpData)

    def getActionOrSubActions(self):
        return (self,)
    
    def actionFinished(self, gripperData=None, tcpData=None):

        if len(self.robot.actionQueue.actions) == 1:
            #last item, need to return data
            gripperData = self.robot._getGripperData()
            tcpData = self.robot._getTCPData()

        raise RobotActionFinishedException(f"Action: {self} has finished normally.", gripperData, tcpData)
