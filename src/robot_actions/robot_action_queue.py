class RobotActionQueue(object):
    def __init__(self):
        self.actions = []


    def __str__(self):
        return f"RobotActionQueue with {len(self.actions)} actions."


    def addActionToQueue(self, robotAction):
        actions = robotAction.getActionOrSubActions()
        for action in actions:
            self.actions.append(action)

    def addActionsToQueue(self, *args):
        for item in args:
            self.addActionToQueue(item)