from dataclasses import dataclass
import pybullet as p
import math
import time
from datetime import date, datetime, timedelta


def _setFingerMotors(robot,targetVelocities, forces=(50,50), positionGains=(0.1,0.1)):
    """sets motors for controlling fingers for every RobotAction where the gripper is involved"""
    p.setJointMotorControlArray(
        robot.robotId,
        robot.FINGER_JOINTS,
        p.VELOCITY_CONTROL,
        targetVelocities=targetVelocities,
        forces=forces,
        positionGains=positionGains
    )

def _setFingerMotorsPositionControl(robot, targetPosition, targetVelocities, forces=(50,50)):
    p.setJointMotorControlArray(
        robot.robotId,
        robot.FINGER_JOINTS,
        p.POSITION_CONTROL,
        targetPositions=targetPosition,
        targetVelocities=targetVelocities,
        forces=forces,
    )


def _stop_finger_motors(robot):
    """stops finger motors after action finished; So next action which isn't usign fingers is not again moving fingers unintentional"""
    p.setJointMotorControlArray(
        robot.robotId,
        robot.FINGER_JOINTS,
        p.VELOCITY_CONTROL,
        targetVelocities=(0,0),
    )





def _setArmMotors(robot,jointPoses):
    """sets motors for arm controlling for every RobotAction where arm movement is involved"""
    for i in range(6):
        p.setJointMotorControl2(
            robot.robotId,
            robot.activeJoints[i],
            p.POSITION_CONTROL,
            jointPoses[i],
            targetVelocity=0,
            force=robot.jointMaxForces[i],
            maxVelocity=robot.jointMaxVelocities[i],
        )

def _calculateIK(robot,pos,orn,useLimits=False,numIter=100,residual=1e-4):
    """calculates IK for every arm movement where IK are involved"""
    if useLimits is True:
        jointPoses = p.calculateInverseKinematics(
            robot.robotId,
            robot.endEffectorLinkIndex,
            pos,
            p.getQuaternionFromEuler(orn),
            lowerLimits=robot.ll,
            upperLimits=robot.ul,
            jointRanges=robot.jr,
            restPoses=robot.restPose,
            maxNumIterations=numIter,
            residualThreshold=residual,)

    else:
        jointPoses = p.calculateInverseKinematics(
            robot.robotId,
            robot.endEffectorLinkIndex,
            pos,
            p.getQuaternionFromEuler(orn),
            maxNumIterations=numIter,
            residualThreshold=residual,)
    
    return jointPoses


def _gripperOpeningWidth(robot):
    f1 = p.getJointState(robot.robotId, robot.FINGER_JOINTS[0])[0]
    f2 = p.getJointState(robot.robotId, robot.FINGER_JOINTS[1])[0]
    return abs(f1-f2)

def _gripperForce(robot):
    force = 0.0
    if len(robot.objects) > 0:

        for obj,objPos in robot.objects:
            left_finger_contact = p.getContactPoints(bodyA=robot.robotId, bodyB=obj, linkIndexA=9)
            right_finger_contact = p.getContactPoints(bodyA=robot.robotId, bodyB=obj, linkIndexA=11)

            if left_finger_contact is None or right_finger_contact is None:
                return 0

            contact = left_finger_contact + right_finger_contact

            if len(contact) > 0:
                for point in contact:
                    force += point[9]
    return force/2