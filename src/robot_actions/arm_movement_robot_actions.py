import pybullet as p
import math
from datetime import date, datetime, timedelta
from .robot_actions_helper_functions import _setFingerMotors, _calculateIK, _setArmMotors ,_stop_finger_motors, _gripperOpeningWidth, _gripperForce
from .robot_action_base import RobotAction, RobotActionFinishedException




class RobotArmMovement(RobotAction):
    """
    Perform arm movement, using inverse kinematics of end effector
    no force on fingers --> not able to move objects. Use 'RobotArmMovementWithLoad' 

    """
    def __init__(self,robot, pos, orn, cancelActionDelay=5):
        RobotAction.__init__(self, robot, cancelActionDelay)

        self.threshold = 1e-3
        self.pos = pos
        self.orn = orn

    def __str__(self):
        return f"RobotArmMovement, pos={self.pos}, orn={self.orn}"
    
    def doAction(self):
        """
        1) calculates the distance between current and desired position of end effector
        2) if close enough, job is finished
        """

        # try:
        super().doAction()
        
        eePos = p.getLinkState(self.robot.robotId, self.robot.endEffectorLinkIndex)[4]

        dis_ee_target = math.sqrt(
            (self.pos[0]-eePos[0])**2
            +(self.pos[1]-eePos[1])**2
            +(self.pos[2]-eePos[2])**2
        )

        if dis_ee_target < self.threshold:
            #desired position reached, 
            self.actionFinished()
        
        jointPoses = _calculateIK(self.robot,self.pos,self.orn)
        _setArmMotors(self.robot,jointPoses[:6])


class RobotArmMovementWithLoad(RobotArmMovement):
    """
    Perform arm movement, using inverse kinematics of end effector
    while clamping fingers --> made for picking & moving part.
    """

    def __init__(self,robot,pos,orn,fingerVelocity=0.1, fingerForce=50, cancelActionDelay=5):
        RobotArmMovement.__init__(self, robot,pos,orn, cancelActionDelay)
        
        self.fingerVelocities = [i*fingerVelocity for i in (-1,1)]
        self.fingerForces = (fingerForce,)*2

    def __str__(self):
        return f"RobotArmMovementWithLoad, pos={self.pos}, orn={self.orn}"

    def doAction(self):
        """
        1) calculates the distance between current and desired position of end effector
        2) if close enough, job is finished
        """
        _setFingerMotors(self.robot,self.fingerVelocities ,self.fingerForces)
        super().doAction()

    def actionFinished(self):
        _stop_finger_motors(self.robot)
        super().actionFinished()

       


