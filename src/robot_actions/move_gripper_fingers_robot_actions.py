import pybullet as p

from datetime import datetime

from .robot_actions_helper_functions import _setFingerMotors, _stop_finger_motors, _gripperOpeningWidth, _gripperForce, _setFingerMotorsPositionControl
from .robot_action_base import RobotAction
from ..joint_forces_data import GripperData, TCPData, Position, OrnEuler, OrnQuaternion

class _MoveGripperFingersBase(RobotAction):
    def __init__(self,robot,isOpening=True,fingerVelocity=0.1, fingerForce=50, cancelActionDelay=5):
        RobotAction.__init__(self, robot, cancelActionDelay)

        self.isOpening = isOpening
        self.fingerForce = fingerForce
        self.fingerForces = (fingerForce,)*2

        if self.isOpening == True:
            #do opening of gripper
            self.fingerVelocities = [i * fingerVelocity for i in (1,-1)]
        else:
            self.fingerVelocities = [i * fingerVelocity for i in (-1,1)]
    
    def __str__(self):
        return f"MoveGripperFingers, isOpening={self.isOpening}, force={self.fingerForce}"

    def _finishAction(self):
        _stop_finger_motors(self.robot)

        ### OLD IMPLEMENTATION, NOW EVERY LAST ACTION IN THE QUEUE IS GOING TO PUBLISH STATE ###
        # gripperData = self.robot._getGripperData()
        # tcpData = self.robot._getTCPData()
        # self.actionFinished(gripperData, tcpData)

        self.actionFinished()

    def doAction(self):
        super().doAction()
        _setFingerMotors(self.robot,self.fingerVelocities, self.fingerForces)

       

class MoveGripperFingersForceControl(_MoveGripperFingersBase):
    
    """
    Move gripper fingers with given velocity and force. Action will finish when: 
    a) after <cancelActionDelay> seconds
    b) when clamping force exceeds 95% of desired clamping force
    

    Parameters:
    isOpening: Direction of finger movement 
    fingerVelocity: Velo of each finger
    fingerForce: Clamping Force
    cancelActionDelay: Duration after which the gripping action should be aborted, in seconds
    """

    def __init__(self, robot, isOpening=False, fingerVelocity=0.1, fingerForce=50, cancelActionDelay=5):
        super().__init__(robot, isOpening=isOpening, fingerVelocity=fingerVelocity, fingerForce=fingerForce, cancelActionDelay=cancelActionDelay)

        self.clampingForceTimer = None
    
    def doAction(self):

        super().doAction()

        if self.isOpening is False:
            #its closing -> finish action based on clamping force

            force = _gripperForce(self.robot)
            if force/self.fingerForce > 0.95:
                if self.clampingForceTimer is None:
                    self.clampingForceTimer = datetime.now()
            
            
            if self.clampingForceTimer is not None and (datetime.now() - self.clampingForceTimer).total_seconds() > 1:
                self._finishAction()
        else:
            distance = _gripperOpeningWidth(self.robot)
            if distance > 0.99*2*0.052:
                self._finishAction()


class MoveGripperFingersDistanceControl(_MoveGripperFingersBase):


    """
    Move gripper fingers with given velocity and force. Action will finish when: 
    a) after <cancelActionDelay> seconds
    b) when <fingerWidthBounderyValue> is reached (in both moving directions)
    

    Parameters:
    isOpening: Direction of finger movement 
    fingerVelocity: Velo of each finger
    fingerForce: Clamping Force
    cancelActionDelay: Duration after which the gripping action should be aborted, in seconds
    fingerWidthBounderyValue: finger width boundery value 
    """

    def __init__(self, robot, isOpening=True, fingerVelocity=0.1, fingerForce=50, cancelActionDelay=5):
        super().__init__(robot, isOpening=isOpening, fingerVelocity=fingerVelocity, fingerForce=fingerForce, cancelActionDelay=cancelActionDelay)
        self.targetPositions = (0.053,-0.053) if self.isOpening is True else (0,0)

        self.clampingTimer = None

    
    def doAction(self):
        super(type(self).__base__, self).doAction()
        _setFingerMotorsPositionControl(self.robot,self.targetPositions, self.fingerVelocities, self.fingerForces)


        distance = _gripperOpeningWidth(self.robot)
        print("--- distance is:", distance)

        if self.isOpening is True:
            if distance > 0.99*2*0.052:
                self._finishAction()
        else:
            if distance < 1e-3:
                if self.clampingTimer is None:
                    self.clampingTimer = datetime.now()
                
                elif (datetime.now() - self.clampingTimer).total_seconds() > 1:
                    self._finishAction()

