# %%
from torch import nn
import torch


class QLearningNN(nn.Module):

    def __init__(self):
            super().__init__()

            self.input_1 = nn.Linear(6,20)
            self.input_2 = nn.Linear(3,10)

            self.hidden_1 = nn.Linear(30, 30)
            self.hidden_2 = nn.Linear(30,20)
            self.hidden_3 = nn.Linear(20,10)

            self.output_layer = nn.Linear(10,1)

    def forward(self, x1, x2):
        x1 = torch.tanh(self.input_1(x1))
        x2 = torch.tanh(self.input_2(x2))
        x = torch.cat((x1, x2), dim=1)

        x = self.hidden_1(x)
        x = self.hidden_2(x)
        x = self.hidden_3(x)

        x = self.output_layer(x)

        return x
        

class QLearningNN2(nn.Module):
    
    def __init__(self):
        super().__init__()
        self.input1 = nn.Linear(6,15)
        self.input2 = nn.Linear(3,10)

        self.h1 = nn.Linear(25,20)
        self.af1 = nn.ReLU()

        self.h2 = nn.Linear(20,10)
        self.af2 = nn.Tanh()

        self.h3 = nn.Linear(10,5)
        self.af3 = nn.ReLU()

        self.output = nn.Linear(5,1)

    
    
    def forward(self, x1, x2):

        x1 = self.input1(x1)
        x2 = self.input2(x2)
        x = torch.cat((x1, x2), dim=1)

        x = self.af1(self.h1(x))
        
        x = self.af2(self.h2(x))

        x = self.af3(self.h3(x))
        
        x = self.output(x)


        return x
    

# %%

if __name__ == "__main__":

    net = QLearningNN2()
    
    x1 = torch.tensor(((-20,10,0.5,30,-2,1),))
    x2 = torch.tensor(((0.01, -0.05, 0.8),))

    print(x1.size())
    print(x2.size())
    

    print(net(x1, x2))