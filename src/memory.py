import torch
import random

class Memory(object):
    
    def __init__(self, batch_size):
        self.batch_size = batch_size
        self.data = []

    def add_data(self, data_tuple):
        self.data.append(data_tuple)

    def get_samples(self):
        if len(self.data) < self.batch_size: return

        # choices_list = random.choices(self.data, k=self.batch_size)
        choices_list = []

        for _ in range(self.batch_size):
            rand_tuple = self.data.pop(random.randint(0,len(self.data)-1))
            choices_list.append(rand_tuple)
        
        state, action, reward = [],[],[]
        for s, a, r in choices_list:
            state.append(s)
            action.append(a)
            reward.append((r,))
        return torch.Tensor(state), torch.Tensor(action), torch.Tensor(reward)