from dataclasses import dataclass

@dataclass
class Position:
    """Data class for positions in cartesian coordinate systems"""
    x: float
    y: float
    z: float

    def get_as_list(self):
        return [self.x, self.y, self.z]


@dataclass
class OrnQuaternion:
    """Data class for quaternion orientations in cartesian coordinate systems"""
    x: float
    y: float
    z: float
    w: float

    def get_as_list(self):
        return [self.x, self.y, self.z, self.w]

@dataclass
class OrnEuler:
    """Data class for euler orientations in cartesian coordinate systems"""
    x: float
    y: float
    z: float

    def get_as_list(self):
        return [self.x, self.y, self.z]

@dataclass
class TCPData:
    """Data class for collecting all tcp-related data such as pos, orn, forces"""
    stepCounter:int
    simulationCounter:int
    fx:float
    fy:float
    fz:float
    mx:float
    my:float
    mz:float
    tcpPos:Position
    tcpOrnQuat: OrnQuaternion
    tcpOrnEuler: OrnEuler

    def __str__(self) -> str:
        return f"""TCP Data: {self.stepCounter} / {self.simulationCounter}
        Forces:     Fx={self.fx}       Fy={self.fy}       Fz={self.fz}
        Torque:     Mx={self.mx}       My={self.my}       Mz={self.mz}
        Tcp position = {self.tcpPos}
        Orn Quat = {self.tcpOrnQuat}
        Orn Euler = {self.tcpOrnEuler}
        """
    def get_forces_tuple(self):
        return (self.fx, self.fy, self.fz, self.mx, self.my, self.mz)


@dataclass
class GripperData:
    """Data class for collecting all gripper-related data such as force and gripper opening width"""
    stepCounter:int
    simulationCounter:int
    force: float
    openingWidth: float
    eeGuidePos:Position
    eeGuideOrnQuat: OrnQuaternion
    eeGuideOrnEuler: OrnEuler

    def __str__(self) -> str:
        return f"""Gripper Data:{self.stepCounter} / {self.simulationCounter}
        force= {self.force}, openingWidth={self.openingWidth} 
        eeGuidePos={self.eeGuidePos}
        OrnQuat = {self.eeGuideOrnQuat}
        OrnEuler = {self.eeGuideOrnEuler}
        """



